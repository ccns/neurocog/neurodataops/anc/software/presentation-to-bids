import argparse
import csv
import os

parser = argparse.ArgumentParser(
        description = "Converts log file of Presentation by Neurobehavioral Systems to BIDS events file.")
parser.add_argument(
    dest = "log_file_path",
    metavar = "LOG_FILE_PATH",
    help = "Path to log file."
)
parser.add_argument(
    dest = "events_output_path",
    metavar = "EVENTS_OUTPUT_PATH",
    help = "Path to directory for output BIDS events file."
)
parser.add_argument(
    '--start-event-code',
    dest = "START_EVENT_CODE",
    default = "start01",
    metavar = "START_EVENT_CODE",
    help = "Event code that marks the task start after syncing with the scanner; default is `start01`."
)
parser.add_argument(
    '--discard',
    dest = "DISCARD",
    default = '199',
    metavar = "DISCARD",
    help = "Lines with the definded Type will be descarded; default is `199`."
)
args = parser.parse_args()

# Event Table header
ET_EVENT_TYPE = 1
ET_CODE = 2
ET_TIME = 3
ET_DURATION = 6
# TODO: Note that certain settings may cause additions to this list to be made [1] - More columns at the end!!!
et_header = ['Trial', 'Event Type', 'Code']

# Read log file into list of lists.
log = []
with open(args.log_file_path, 'r') as log_file:
    for line in log_file:
        # Split line into fields by `\t`.
        log.append(line.rstrip().split('\t'))

# Process the log and collect events.
events = []
log_it = iter(log)
for l in log_it:
    # Match the Event Table header and process the Event Table only.
    if l[:3] == et_header:
        print('[INFO] We are in Event Table, line: ', l)
        l = next(log_it)
        print('[INFO] Empty line, line: ', l) # Empty line between the header and contents of Event Table.
        l = next(log_it)
        print('[INFO] First table row, line: ', l)
        while len(l) > 1: # Splitting empty string gives a list with one empty-string element. Event Table ends with empty line.
            events.append(l)
            # In case there is no Stimulus Table, the last line of Event Table ends the file and `next` raises StopIteration exception.
            try:
                l = next(log_it)
            except StopIteration:
                break
        break

# Set onsets relative to start time.
# TODO: Convert to int when collecting.
# TODO: Try not to scan the events too many times.
start_time = 0
for e in events:
    if e[ET_CODE] == args.START_EVENT_CODE:
        start_time = int(e[ET_TIME])
        break
for e in events[:]:
    if e[ET_CODE] == args.DISCARD:
        events.remove(e)
    else:
        e[ET_TIME] = str(int(e[ET_TIME]) - start_time)

# Events file header
events_header = ['onset', 'duration', 'code']

# Create new file name:
name = os.path.basename(args.log_file_path).replace(".log", "_events.tsv")

# Create output path if it does not exist:
if not os.path.exists(args.events_output_path):
    os.makedirs(args.events_output_path)

# Write the output file.
with open(os.path.join(args.events_output_path, name), 'w', newline='') as events_file:
    events_writer = csv.writer(events_file, delimiter='\t')
    events_writer.writerow(events_header)
    for e in events:
        # print(e)
        if e[ET_EVENT_TYPE] == 'Picture':
            events_writer.writerow([round(int(e[ET_TIME]) * 0.0001, 4), round(int(e[ET_DURATION]) * 0.0001, 4), e[ET_CODE]])
        elif e[ET_EVENT_TYPE] == 'Manual': # Manually-added entries to the log file.
            continue
        else: # Events other than `Picture` don't have duration.
            events_writer.writerow([round(int(e[ET_TIME]) * 0.0001, 4), 0.000, e[ET_CODE]])
