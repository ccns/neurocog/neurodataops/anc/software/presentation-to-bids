# Presentation logs to BIDS events converter

Convert a log file by Neurobehavioral Systems Presentation software to BIDS events TSV file. Currently only one study has been tested and there are TODOs pointing to variations in the [Event Table](https://www.neurobs.com/pres_docs/html/03_presentation/07_data_reporting/01_logfiles/03_event_table.htm) schema.

### Usage information


```
python3 pres2bids.py <path-to-presentation-log>
```

The presentation log should contain an event table without any custom properties.
